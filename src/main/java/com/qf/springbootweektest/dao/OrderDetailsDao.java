package com.qf.springbootweektest.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.springbootweektest.entity.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;


public interface OrderDetailsDao extends BaseMapper<OrderDetails> {

}
