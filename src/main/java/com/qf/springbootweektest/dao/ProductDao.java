package com.qf.springbootweektest.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.springbootweektest.entity.Product;
import com.qf.springbootweektest.vo.ProductVo;


public interface ProductDao extends BaseMapper<Product> {

}
