package com.qf.springbootweektest.controller;

import com.qf.springbootweektest.service.ProductService;
import com.qf.springbootweektest.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
    @GetMapping("/bar")
    public ProductVo getBar(){
        ProductVo productVo = productService.getBar();
        return productVo;
    }

}
