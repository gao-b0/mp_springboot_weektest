package com.qf.springbootweektest.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.springbootweektest.entity.OrderDetails;
import com.qf.springbootweektest.entity.Product;
import com.qf.springbootweektest.vo.ProductVo;

public interface ProductService extends IService<Product> {
    ProductVo getBar();
}
