package com.qf.springbootweektest.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.springbootweektest.dao.OrderDetailsDao;
import com.qf.springbootweektest.dao.ProductDao;
import com.qf.springbootweektest.entity.OrderDetails;
import com.qf.springbootweektest.entity.Product;
import com.qf.springbootweektest.service.ProductService;
import com.qf.springbootweektest.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class ProductServiceImpl extends ServiceImpl<ProductDao, Product> implements ProductService {
    @Autowired
    ProductDao productDao;
    @Autowired
    OrderDetailsDao orderDetailsDao;
    @Override
    public ProductVo getBar() {
        List<Product> products = productDao.selectList(null);
        ArrayList<String> categories = new ArrayList<>();
        ArrayList<Object> values = new ArrayList<>();
        for (Product product : products) {
            categories.add(product.getPname());
        }
        List<OrderDetails> orderDetails = orderDetailsDao.selectList(null);
        for (OrderDetails orderDetail : orderDetails) {
            values.add(orderDetail.getSales());
        }
        ProductVo productVo = new ProductVo();
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("categories", categories);
        map.put("values",values);
        productVo.setData(map);
        return productVo;
    }
}
