package com.qf.springbootweektest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.qf.springbootweektest.dao")
public class SpringbootWeektestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootWeektestApplication.class, args);
    }

}
